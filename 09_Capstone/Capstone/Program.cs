﻿using Capstone.DAL;
using Capstone.Views;
using Microsoft.Extensions.Configuration;
using System;
using System.IO;

namespace Capstone
{
    class Program
    {
        static void Main(string[] args)
        {
            // Get the connection string from the appsettings.json file
            IConfigurationBuilder builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

            IConfigurationRoot configuration = builder.Build();

            string connectionString = configuration.GetConnectionString("Project");



            // Create a menu and run it
            ParkSqlDAO parkDAO = new ParkSqlDAO(connectionString);
            SiteSqlDAO siteDAO = new SiteSqlDAO(connectionString);
            ReservationSqlDAO reservationDAO = new ReservationSqlDAO(connectionString);
            CampgroundSqlDAO campgroundDAO = new CampgroundSqlDAO(connectionString);

            MainMenu menu = new MainMenu(parkDAO, siteDAO, reservationDAO, campgroundDAO);

            menu.Run();
        }
    }
}
