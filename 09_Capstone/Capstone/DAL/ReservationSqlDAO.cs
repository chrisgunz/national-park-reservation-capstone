﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using Capstone.Models;

namespace Capstone.DAL
{
    public class ReservationSqlDAO : IReservationDAO
    {
        private string connectionString;

        public ReservationSqlDAO(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public int CreateReservation(int siteId, DateTime arrivalDate, DateTime departureDate, string name)
        {
            int reservationId = 0;
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    string sql = "insert into reservation (site_id, name, from_date, to_date, create_date)" +
                        "values (@siteid, @name, @arrival, @departure, GETDATE()); select @@identity as reservationId";
                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.Parameters.AddWithValue("@arrival", arrivalDate);
                    cmd.Parameters.AddWithValue("@departure", departureDate);
                    cmd.Parameters.AddWithValue("@siteid", siteId);
                    cmd.Parameters.AddWithValue("@name", name);
                    reservationId = Convert.ToInt32(cmd.ExecuteScalar());
                    return reservationId;
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine($"There was a ERROR {ex.Message}");
            }
            return reservationId;
        }

        public int GetTotalReservationCount()
        {
            int reservationCount = 0;
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    string sql = "select count(*) from reservation";
                    SqlCommand cmd = new SqlCommand(sql, conn);
                    reservationCount = Convert.ToInt32(cmd.ExecuteScalar());
                    return reservationCount;
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine($"There was a ERROR {ex.Message}");
            }
            return reservationCount;
        }
    }
}
