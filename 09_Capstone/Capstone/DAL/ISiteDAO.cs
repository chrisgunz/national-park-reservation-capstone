﻿using Capstone.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Capstone.DAL
{
    public interface ISiteDAO
    {
        IList<Site> SelectSitesByCampgroundId(int campgroundid);

        IList<Site> SelectAvaliableSites(int campgroundid, DateTime arrivaldate, DateTime departuredate);
    }

}
