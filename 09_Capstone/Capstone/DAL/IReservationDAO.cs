﻿using Capstone.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Capstone.DAL
{
    public interface IReservationDAO
    {
        int CreateReservation(int siteId, DateTime arrivalDate, DateTime departureDate, string name);
    }
}
