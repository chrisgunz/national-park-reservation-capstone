﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using Capstone.Models;

namespace Capstone.DAL
{
    public class ParkSqlDAO : IParkDAO
    {
        private string connectionString;

        // Single Parameter Constructor
        public ParkSqlDAO(string dbConnectionString)
        {
            connectionString = dbConnectionString;
        }

        public IList<Park> GetAllParks()
        {
            List<Park> list = new List<Park>();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    string sql = "SELECT * FROM park order by name ASC";
                    SqlCommand cmd = new SqlCommand(sql, conn);
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        Park obj = new Park();
                        obj.Area = Convert.ToInt32(reader["area"]);
                        obj.Name = Convert.ToString(reader["name"]);
                        obj.Description = Convert.ToString(reader["description"]);
                        obj.EstablishDate = Convert.ToDateTime(reader["establish_date"]);
                        obj.Id = Convert.ToInt32(reader["park_id"]);
                        obj.Location = Convert.ToString(reader["location"]);
                        obj.Visitors = Convert.ToInt32(reader["visitors"]);

                        list.Add(obj);
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine($"There was a ERROR {ex.Message}");
            }
            return list;
        }

        public int GetParkIdByName(string parkName)
        {
            int parkId = 0;
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    string sql = "SELECT park_id FROM park where name = @parkname";
                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.Parameters.AddWithValue("@parkname", parkName);
                    parkId = Convert.ToInt32(cmd.ExecuteScalar());
                    
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine($"There was a ERROR {ex.Message}");
            }
            return parkId;
        }
    }
}
