﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using Capstone.Models;

namespace Capstone.DAL
{
    public class CampgroundSqlDAO : ICampgroundDAO
    {
        private string connectionString;

        public CampgroundSqlDAO(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public IList<Campground> GetCampgroundsByParkId(int parkid)
        {
            List<Campground> list = new List<Campground>();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    string sql = "SELECT * FROM campground where park_id = @parkid order by name ASC";
                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.Parameters.AddWithValue("@parkid", parkid);
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        Campground obj = new Campground();
                        obj.Id= Convert.ToInt32(reader["campground_id"]);
                        obj.Name = Convert.ToString(reader["name"]);
                        obj.OpenFromMonth = Convert.ToInt32(reader["open_from_mm"]);
                        obj.OpenToMonth = Convert.ToInt32(reader["open_to_mm"]);
                        obj.DailyFee = Convert.ToInt32(reader["daily_fee"]);
                      

                        list.Add(obj);
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine($"There was a ERROR {ex.Message}");
            }
            return list;
        }
    }
}
