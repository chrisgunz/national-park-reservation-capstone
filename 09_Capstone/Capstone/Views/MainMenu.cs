﻿using Capstone.DAL;
using Capstone.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Capstone.Views
{
    /// <summary>
    /// The top-level menu in our Market Application
    /// </summary>
    public class MainMenu : CLIMenu
    {
        IList<Park> AllParks;
        /// <summary>
        /// Constructor adds items to the top-level menu
        /// </summary>
        public MainMenu(ParkSqlDAO parkDAO,SiteSqlDAO siteDAO,ReservationSqlDAO reservationDAO,CampgroundSqlDAO campgroundDAO ) : base(parkDAO, siteDAO, reservationDAO, campgroundDAO)
        {
            this.Title = "*** Select a Park for farther details..***";
            this.AllParks = this.ParkDAO.GetAllParks();
            for (int i = 1; i <= this.AllParks.Count; i++)
            {
                this.menuOptions.Add(Convert.ToString(i), this.AllParks[i - 1].Name); 
            }
    
            this.menuOptions.Add("Q", "Quit");
        }

        /// <summary>
        /// The override of ExecuteSelection handles whatever selection was made by the user.
        /// This is where any business logic is executed.
        /// </summary>
        /// <param name="choice">"Key" of the user's menu selection</param>
        /// <returns></returns>
        protected override bool ExecuteSelection(string choice)
        {
            choice = choice.Trim();
            if (choice.ToUpper() == "Q")
            {
                return false;
            }
            else if (Convert.ToInt32(choice) > this.AllParks.Count)
            {
                Console.WriteLine("Invalid choice. Please try again.");
                return true;
            }
            {
                Park parkToOpen = this.AllParks[Convert.ToInt32(choice) - 1];
                ParkMenu parkMenu = new ParkMenu(parkToOpen, this.ParkDAO, this.SiteDAO, this.ReservationDAO, this.CampgroundDAO);
                parkMenu.Run();
            }
            return true;
        }

        protected override void DisplayBeforeMenu()
        {
            Console.WriteLine(this.Title);
            Console.WriteLine(new string('=', this.Title.Length));
            Console.WriteLine();
            Console.WriteLine("\r\nPlease make a selection:");
        }

        protected override void DisplayAfterMenu()
        {
        }

    }
}
