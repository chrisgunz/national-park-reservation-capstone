﻿using Capstone.DAL;
using Capstone.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Capstone.Views
{
    public class ReservationMenu : CLIMenu
    {
      
        Park Park;
        private IList<Campground> AllCampgrounds;

        public ReservationMenu(Park park, ParkSqlDAO parkDAO, SiteSqlDAO siteDAO, ReservationSqlDAO reservationDAO, CampgroundSqlDAO campgroundDAO) : base(parkDAO, siteDAO, reservationDAO, campgroundDAO)
        {
            this.Title = $"Reservations for {park.Name} National Park";
            this.Park = park;
            this.AllCampgrounds = this.CampgroundDAO.GetCampgroundsByParkId(Park.Id);
            for (int i = 1; i <= this.AllCampgrounds.Count; i++)
            {
                Campground currentcamp = this.AllCampgrounds[i - 1];
                this.menuOptions.Add(Convert.ToString(i), string.Format("{0,-20}\t{1,-15}\t{2,-15}\t{3,-8}",
                    currentcamp.Name,
                    CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(currentcamp.OpenFromMonth),
                    CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(currentcamp.OpenToMonth),
                    $"{currentcamp.DailyFee:C}"));
            }
        }

        protected override bool ExecuteSelection(string choice)
        {
            if (choice.Trim().ToUpper() == "Q" || Convert.ToInt32(choice) == 0)
            {
                return false;
            }
            else if (Convert.ToInt32(choice) > this.AllCampgrounds.Count)
            {
                Console.WriteLine("Invalid selection. Please try again.");
            }
            else
            {
                Console.WriteLine("What is the arrival date?(format mm/dd/yyyy)");
                DateTime arrivalDate = Convert.ToDateTime(Console.ReadLine());
                Console.WriteLine("What is the departure date?(format mm/dd/yyyy)");
                DateTime departureDate = Convert.ToDateTime(Console.ReadLine());
                // if one or both dates are in the past, exit
                // if departure date is before arrival date, exit
                if (arrivalDate < DateTime.Now || departureDate < DateTime.Now || departureDate < arrivalDate)
                {
                    Console.WriteLine("Invalid date selections. Please try again.");
                    Pause("");
                    return true;
                }

                // get campground
                Campground currentCamp = this.AllCampgrounds[Convert.ToInt32(choice) - 1];
                IList<Site> sites = this.SiteDAO.SelectAvaliableSites(currentCamp.Id, Convert.ToDateTime(arrivalDate), Convert.ToDateTime(departureDate));
                if (sites.Count == 0)
                {
                    Console.WriteLine("No reservations available for your date selection. Please try again.");
                    Pause("");
                    return true;
                }
                else
                {
                    Console.WriteLine("Results Matching Your Search Criteria");
                    Console.WriteLine(string.Format("{0, -15}\t{1,-15}\t{2,-15}\t{3,-15}\t{4,-15}\t{5,-15}",
                        "Site No.",
                        "Max Occup.",
                        "Accessible?",
                        "Max RV Length",
                        "Utility",
                        "Cost"));
                    foreach (Site site in sites)
                    {
                        string accessible = "No";
                        if (site.IsAccessible)
                        {
                            accessible = "Yes";
                        }
                        string maxRvLength = "N/A";
                        if (site.MaxRVLength > 0)
                        {
                            maxRvLength = Convert.ToString(site.MaxRVLength);
                        }
                        string utilities = "No";
                        if (site.HasUtilities)
                        {
                            utilities = "Yes";
                        }
                        int lengthOfStay = (departureDate - arrivalDate).Days;
                        decimal costOfStay = currentCamp.DailyFee * lengthOfStay;
                        Console.WriteLine(string.Format("{0, -15}\t{1,-15}\t{2,-15}\t{3,-15}\t{4,-15}\t{5,-15}",
                            site.SiteNumber,
                            site.MaxOccupancy,
                            accessible,
                            maxRvLength,
                            utilities,
                            $"{costOfStay:C}"));
                    
          
                    }
                    int siteNumber = this.GetInteger("Which site should be reserved? (enter 0 to cancel)");
                    int siteId = 0;
                    foreach (Site choiceSite in sites)
                    {
                        if (siteNumber == choiceSite.SiteNumber)
                        {
                            siteId = choiceSite.Id;
                        }
                    }
                    if (siteId == 0)
                    {
                        
                        Pause("entered a invalid site please try again");
                        return true;
                    }
                    string reservationName = this.GetString("What name should the reservation be made under?");
                    int newReservationId = this.ReservationDAO.CreateReservation(siteId, arrivalDate, departureDate, reservationName);
                    Console.WriteLine();
                    Console.WriteLine($"the reservation has been made and the confirmation Id is {newReservationId}");
                    Pause("");
                    return true;

                }

                return true;
            }
            return true;
        }
        protected override void DisplayBeforeMenu()
        {
            Console.WriteLine(this.Title);
             
            Console.WriteLine(string.Format("\t{0, -20}\t{1,-15}\t{2,-15}\t{3,-8}",
                                "Name",
                                "Open",
                                "Close",
                                "Daily Fee"));
        }
        protected override void DisplayAfterMenu()
        {
            Console.WriteLine("Which campground (enter 0 to cancel)?");
        }
    }
}
