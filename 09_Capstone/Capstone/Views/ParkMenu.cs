﻿using Capstone.DAL;
using Capstone.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Capstone.Views
{
    public class ParkMenu : CLIMenu
    {
        public Park Park;

        public ParkMenu(Park park, ParkSqlDAO parkDAO, SiteSqlDAO siteDAO, ReservationSqlDAO reservationDAO, CampgroundSqlDAO campgroundDAO) : base(parkDAO, siteDAO, reservationDAO, campgroundDAO)
        {
            this.Park = park;
            this.Title = park.Name;
            this.menuOptions.Add("1", "View Campgrounds");
            this.menuOptions.Add("2", "Search for Reservation");
            this.menuOptions.Add("3", "Return to Previous Screen");
        }
        protected override bool ExecuteSelection(string choice)
        {
            switch (choice)
            {
                case "1":
                    CampgroundMenu campgroundMenu = new CampgroundMenu(this.Park, this.ParkDAO, this.SiteDAO, this.ReservationDAO, this.CampgroundDAO);
                    campgroundMenu.Run();
                    return true;
                case "2":
                    // open the reservation menu
                    ReservationMenu menu = new ReservationMenu(this.Park, this.ParkDAO, this.SiteDAO, this.ReservationDAO, this.CampgroundDAO);
                    menu.Run();
                    return true;
                case "3":
                    return false;
            }

            return true;
        }

        protected override void DisplayBeforeMenu()
        {
            // Here is where we will display all the park info for one park
            Console.WriteLine($"{this.Park.Name} National Park");
            Console.WriteLine(string.Format(
                    "{0,-20}\t{1,-15}", "Location: ", this.Park.Location));
            Console.WriteLine(string.Format(
                    "{0, -20}\t{1,-15}", "Established: ", this.Park.EstablishDate.ToShortDateString()));
            Console.WriteLine(string.Format(
                    "{0, -20}\t{1,-15}", "Area: ", this.Park.Area.ToString("N0")  +" sq km"));
            Console.WriteLine(string.Format(
                    "{0, -20}\t{1,-15}", "Annual Visitors: ", this.Park.Visitors.ToString("N0")));
            Console.WriteLine();
            Console.WriteLine(this.Park.Description);
            Console.WriteLine("\r\nPlease make a selection:");
        }
    }

}
