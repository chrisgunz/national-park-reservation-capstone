﻿using Capstone.DAL;
using Capstone.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Capstone.Views
{
    public class CampgroundMenu : CLIMenu
    {
        public Park Park;

        public CampgroundMenu(Park park, ParkSqlDAO parkDAO, SiteSqlDAO siteDAO, ReservationSqlDAO reservationDAO, CampgroundSqlDAO campgroundDAO) : base(parkDAO, siteDAO, reservationDAO, campgroundDAO)
        {
            this.Park = park;
            this.Title = park.Name + " National Park Campgrounds";
            this.menuOptions.Add("1", "Search for available reservations");
            this.menuOptions.Add("2", "Return to previous screen");
        }

        protected override bool ExecuteSelection(string choice)
        {
            switch (choice)
            {
                case "1":
                    // Make new reservation menu
                    ReservationMenu menu = new ReservationMenu(this.Park, this.ParkDAO, this.SiteDAO, this.ReservationDAO, this.CampgroundDAO);
                    menu.Run();
                    return true;
                case "2":
                    return false;
            }

            return true;
        }

        protected override void DisplayBeforeMenu()
        {
            Console.WriteLine(this.Title);
            // display all campgrounds
            IList<Campground> AllCampgrounds = this.CampgroundDAO.GetCampgroundsByParkId(Park.Id);
            Console.WriteLine(string.Format("\t{0, -20}\t{1,-15}\t{2,-15}\t{3,-8}",
                    "Name",
                    "Open",
                    "Close",
                    "Daily Fee"));
            for (int i = 1; i <= AllCampgrounds.Count; i++)
            {
                Campground currentcamp = AllCampgrounds[i - 1];

                Console.WriteLine(string.Format("#{0, -5}\t{1,-20}\t{2,-15}\t{3,-15}\t{4,-8}",
                    i,
                    currentcamp.Name,
                    CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(currentcamp.OpenFromMonth),
                    CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(currentcamp.OpenToMonth),
                    $"{currentcamp.DailyFee:C}"));
            }
            Console.WriteLine("\r\nPlease make a selection:");
        }
    }
}
