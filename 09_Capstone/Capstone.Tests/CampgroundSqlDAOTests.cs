using Capstone.DAL;
using Capstone.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace Capstone.Tests
{
    [TestClass]
    public class CampgroundSqlDAOTests : DAOTests
    {
        [TestMethod]
        public void GetCampgroundsForAcadiaIdReturnsTwoCampgrounds()
        {
            CampgroundSqlDAO campgroundDAO = new CampgroundSqlDAO(this.connectionString);
            ParkSqlDAO parkDAO = new ParkSqlDAO(this.connectionString);

            IList<Campground> campgrounds = campgroundDAO.GetCampgroundsByParkId(this.acadiaId);

            Assert.AreEqual(2, campgrounds.Count);
        }
    }
}
