﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Transactions;

namespace Capstone.Tests
{
    [TestClass]
    public class DAOTests
    {
        protected string connectionString = "Server=.\\SqlExpress; Database=npcampground; Trusted_Connection=true;";
        private TransactionScope transaction;
        protected int acadiaId = 0;
        protected int blackwoodsid = 0;
        protected int site1id = 0;
        [TestInitialize]
        public void Setup()
        {
            transaction = new TransactionScope();
            string script = File.ReadAllText("campground-test.sql");

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(script, conn);

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    acadiaId = Convert.ToInt32(reader["acadiaid"]);
                    blackwoodsid = Convert.ToInt32(reader["blackwoodsid"]);
                    site1id = Convert.ToInt32(reader["site1id"]);
                }
            }
        }

        [TestCleanup]
        public void Cleanup()
        {
            this.transaction.Dispose();
        }
    }
}
