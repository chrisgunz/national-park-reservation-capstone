﻿using Capstone.DAL;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace Capstone.Tests
{
    [TestClass]
    public class ReservationSqlDAOTests : DAOTests
    {
        [TestMethod]
        public void GetTotalReservationCountShouldReturnSixteen()
        {
            ReservationSqlDAO dao = new ReservationSqlDAO(this.connectionString);

            int count = dao.GetTotalReservationCount();

            Assert.AreEqual(16, count);
        }

        [TestMethod]
        public void CreateReservationShouldIncreaseReservationCountByOne()
        {
            ReservationSqlDAO dao = new ReservationSqlDAO(this.connectionString);
            int beforeCount = dao.GetTotalReservationCount();

            dao.CreateReservation(this.site1id, Convert.ToDateTime("06/26/2019"), 
                Convert.ToDateTime("06/28/2019"), "Bentivegna Family Reservation");

            Assert.AreEqual(beforeCount + 1, dao.GetTotalReservationCount());
        }
    }
}
