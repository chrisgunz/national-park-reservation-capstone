﻿using Capstone.DAL;
using Capstone.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace Capstone.Tests
{
    [TestClass]
    public class ParkSqlDAOTests : DAOTests
    {
        [TestMethod]
        public void GetAllParksShouldReturnTwo()
        {
            ParkSqlDAO dao = new ParkSqlDAO(this.connectionString);

            IList<Park> list = dao.GetAllParks();

            Assert.AreEqual(2, list.Count);

        }
    }
}
